const ROSLIB = require('roslib')
const QUEUE = require('block-queue')

const logger = require('../../config/winston')('sendgoal_action.js');

class SendGoalAction {
    constructor() {
        this.ros = null;
        this.goal = null;

    }

    establishRos = () => {
        this.ros = new ROSLIB.Ros({
            url : 'ws://localhost:9090'
        });

        this.ros.on('connection', function () {
            console.log('Connected to websocket server.');
        });

        this.ros.on('error', function (error) {
            console.log('Error connecting to websocket server: ', error);
        });

        this.ros.on('close', function () {
            console.log('Connection to websocket server closed.');
        });
    }

    gotoGoal = (pose, orientation) => {
        let actionClient = new ROSLIB.ActionClient({
            ros : this.ros,
            serverName : '/move_base',
            actionName : 'move_base_msgs/MoveBaseAction'
        });

        let poseV = new ROSLIB.Vector3(null);
        let orientationV = new ROSLIB.Quaternion({
            x : 0.0,
            y : 0.0,
            z : orientation.z, 
            w : orientation.w
        });

        let pose = new ROSLIB.Pose({
            position : poseV,
            orientation : orientationV
        });

        this.goal = new ROSLIB.Goal({
            actionClient : actionClient,
            goalMessage : {
                target_pose : {
                    header : {
                        frame_id : 'map'
                    },
                    pose : pose
                }
            }
        });

        let movebaseListener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/move_base/result',
            messageType : 'move_base_msgs/MoveBaseActionResult'
        });

        movebaseListener.subscribe((actionResult) => {
            console.log('Received message on ' + move_baseListener.name + 'status: ' + actionResult.status.status);
        });

    }
}


let sga = new SendGoalAction();