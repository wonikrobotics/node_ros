const ROSLIB = require('roslib')
const QUEUE = require('block-queue')

class SendGoal {
    constructor() {

        this.ros = new ROSLIB.Ros({
            url : 'ws://localhost:9090'
        });


        this.ros.on('connection', ()=> {
            console.log('connected to ros');
            this.onConnected();
        });

        this.ros.on('error', (error) => {
            console.error("cannot connect to ros : ", error);
        });


    }

    onConnected = () => {
        this.moveBatch();
    }


    moveBatch = async () => {
        const POINTS = [
            [1.46434593201, 4.37705039978],
            [1.89934539795, 7.93033313751],
            [10.6828966141, 11.2297134399],
            [9.29943656921, 2.24972057343],
        ]


        for(const element of POINTS) {
            var x = element[0];
            var y = element[1];
            let result = await this.gotoGoal(x, y);
            console.log("result : ", result);
        }
    }

    gotoGoal = (x, y) => {


        return new Promise((resolve, reject) => {
            let actionClient = new ROSLIB.ActionClient({
                ros : this.ros,
                serverName : '/robot_0/move_base',
                actionName : 'move_base_msgs/MoveBaseAction'
            });
    
            var positionVec3 = new ROSLIB.Vector3(null);
            var orientation = new ROSLIB.Quaternion({x:0, y:0, z:0, w:1.0});
    
            positionVec3.x = x;
            positionVec3.y = y;
            var pose = new ROSLIB.Pose({
                position : positionVec3,
                orientation : orientation
            });
    
            var goal = new ROSLIB.Goal({
                actionClient : actionClient,
                goalMessage : {
                  target_pose : {
                    header : {
                      frame_id : 'map'
                    },
                    pose : pose
                  }
                }
            });
    
            goal.send();
            console.log("goal was sent");
    
            // Monitoring /move_base/result
            var move_baseListener = new ROSLIB.Topic({
                ros : this.ros,
                name : '/robot_0//move_base/result',
                messageType : 'move_base_msgs/MoveBaseActionResult'
            });
    
    
            move_baseListener.subscribe((actoinResult) => {
                if(actoinResult.status.status == 3) {
                    resolve(3);
                } else {
                    reject(actoinResult.status.status);
                }
            });
    
        })
        
    }

}

const goal = new SendGoal();


