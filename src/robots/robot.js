
const logger = require('../../config/winston')('robot.js');
const ROSLIB = require('roslib')
const POI_INFO = require("../../config/pois.json")

const MOCK_POSE = {
    pose : {
        x : 0.0,
        y : 0.0,
        z : 0.0,
    },
    orientation : {
        z : 0.0,
        w : 0.0
    }
}

class Robot {
    constructor(config) {
        this.name = config.name;
        this.pose = {}

        this.rosbridgeUrl = config.bridge_url;


        this.topics = {
            poseTopic : `/${this.name}/amcl_pose`
        }


        this.poseInfo = MOCK_POSE;

        this.establishRos();
        this.startPoseListener();


        logger.info(`${this.name} is created`);

        // this.poseSendInterval = setInterval(this.poseSendInterval, 1000);

    }

    establishRos = () => {
        this.ros = new ROSLIB.Ros( {
            url : this.rosbridgeUrl
        });

        this.ros.on('connection', () => {
            // console.log('Connected to websocket server');
            logger.info("Connected to websocket server ")
        });

        this.ros.on('error', (error) => {
            logger.error(`Error connecting to websocket server: ${error}`);
            throw Error(`Error connecting to websocket server: ${error}`);
        });

        this.ros.on('close', () => {
            logger.info('Connection to websocket server closed.');
            // console.linfo('Connection to websocket server closed.');
        });

        this.poseListener = null;

        this.startPoseListener();
    }

    
    startPoseListener = () => {
        const POSE_TOPIC = this.topics.poseTopic;
        this.poseListener = new ROSLIB.Topic({
            ros : this.ros,
            name : `/${this.name}/amcl_pose`,
            messageType : 'geometry_msgs/PoseWithCovarianceStamped'
        });

        this.poseListener.subscribe((message) => {
            // console.log('Received message on ' + this.poseListener.name + ': ' + message.pose.pose.position.x);

            let poseInfo = {
                pose : {
                    x : message.pose.pose.position.x,
                    y : message.pose.pose.position.y
                },
                orientation : {
                    z : message.pose.pose.orientation.z,
                    w : message.pose.pose.orientation.w
                }
            }
            this.poseInfo = poseInfo;
        });
    }


    poseSendInterval = () => {
        if(this.poseInfo != MOCK_POSE) {
            logger.info(`send pose to acs.\n ${JSON.stringify(this.poseInfo, null, 2)} but currently mock`);
        } else {
            console.info("currently mock reference");
        }
    }


    moveBase = async (pose, orientation) => {
        let moveResult = await this.gotoGoal(pose, orientation);
        console.log("move result : ", moveResult);
    }



    gotoGoal = (poseV, orientationV) => {
        // console.log("11111 : ", pose, " : ", orientation)
        return new Promise((resolve, reject) => {
            let actionClient = new ROSLIB.ActionClient({
                ros : this.ros,
                serverName : '/robot_0/move_base',
                actionName : 'move_base_msgs/MoveBaseAction'
            });
    
            var positionVec3 = new ROSLIB.Vector3(null);
            var orientation = new ROSLIB.Quaternion({x:0, y:0, z:0, w:1.0});
    
            positionVec3.x = poseV.x;
            positionVec3.y = poseV.y;

            var pose = new ROSLIB.Pose({
                position : positionVec3,
                orientation : orientationV
            });
    
            var goal = new ROSLIB.Goal({
                actionClient : actionClient,
                goalMessage : {
                  target_pose : {
                    header : {
                      frame_id : 'map'
                    },
                    pose : pose
                  }
                }
            });
    
            goal.send();
            console.log("goal was sent");
    
            // Monitoring /move_base/result
            var move_baseListener = new ROSLIB.Topic({
                ros : this.ros,
                name : '/robot_0//move_base/result',
                messageType : 'move_base_msgs/MoveBaseActionResult'
            });
    
    
            move_baseListener.subscribe((actoinResult) => {
                if(actoinResult.status.status == 3) {
                    resolve(3);
                } else {
                    reject(actoinResult.status.status);
                }
            });
    
        })
        
    }    

}


module.exports = Robot;


// let robot = new Robot({
//     name : 'robot_0',
//     bridge_url : "ws://localhost:9090"
// });


// let pose = { 
//     x : 9.67229391277,
//     y : 7.39138668115
// }

// let orientation = {
//     x : 0.0,
//     y : 0.0,
//     z : -0.681519168493,
//     w : 0.731800261667
// }


// let pose = { 
//     x : 16.7985988734,
//     y : 13.3544072579
// }

// let orientation = {
//     x : 0.0,
//     y : 0.0,
//     z : -0.681519168493,
//     w : 0.731800261667
// }

// robot.moveBase(pose, orientation)




