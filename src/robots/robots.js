
const logger = require('../../config/winston')('robot.js');
const ROBOT_INFO = require("../../config/robot_info.json")
const ROBOT = require("./robot");

class Robots {
    constructor() {
        this.robots = [];
        this.robotDict = {};
        this.robotNameList = ROBOT_INFO.robot_list;

        this.createRobots();

        // this is test commit comment

        
    }


    createRobots = () => {
        this.robotNameList.forEach((item) => {
            let robot = new ROBOT({
                name : item
            });

            this.robotDict[item] = robot;
            this.robots.push(item);

        });
    }

    getRobots = () => {
        return this.robots;
    }

    getRobotDict = () => {
        return this.robotDict;
    }


    getRobotByName = (name) => {
        return this.robotDict.name;
    }


}


let robots = new Robots();
// let robotdict = robots.getRobotDict();


